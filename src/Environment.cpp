/**
 * @file Environment.cpp
 * @brief Implementation of the Environment class methods.
 *
 * The Environment class models the environmental conditions affecting the rabbit population.
 * It includes methods to set and retrieve season-specific parameters such as VHD and Myxomatose
 * probabilities and death probabilities. This file contains the implementation of the Environment
 * class methods, including the constructor and methods for setting and getting environmental parameters.
 */

#include "Environment.h"

/**
 * @brief Constructor for the Environment class.
 *
 * Initializes the environment with default values for season and probabilities.
 */
Environment::Environment() : season(-1), VHDProbability(0), MyxomatoseProbability(0), deathProbability(0){}

/**
 * @brief Get the current season.
 *
 * Returns the current season, where 0 represents Winter, 1 represents Spring,
 * 2 represents Summer, and 3 represents Autumn.
 *
 * @return The current season.
 */
int Environment::getSeason() const
{
    return season;
}

/**
 * @brief Set environmental parameters for Winter.
 *
 * Sets the season to Winter and adjusts VHD, Myxomatose, and death probabilities
 * based on the number of occurrences in the rabbit population.
 *
 * @param numVHD The number of rabbits affected by VHD.
 * @param numMyxomatose The number of rabbits affected by Myxomatose.
 * @param numRabbit The total number of rabbits in the population.
 */
void Environment::setWinter(int numVHD, int numMyxomatose, int numRabbit)
{
    season = 0;
    MyxomatoseProbability = 0.001 + double(numMyxomatose / numRabbit);
    VHDProbability = 0.03 + double(numVHD / numRabbit);
    deathProbability = 0.05;
}

/**
 * @brief Set environmental parameters for Spring.
 *
 * Sets the season to Spring and adjusts VHD, Myxomatose, and death probabilities
 * based on the number of occurrences in the rabbit population.
 *
 * @param numVHD The number of rabbits affected by VHD.
 * @param numMyxomatose The number of rabbits affected by Myxomatose.
 * @param numRabbit The total number of rabbits in the population.
 */
void Environment::setSpring(int numVHD, int numMyxomatose, int numRabbit)
{
    season = 1;
    MyxomatoseProbability = 0.001 + double(numMyxomatose / numRabbit);
    VHDProbability = 0.001 + double(numVHD / numRabbit);
    deathProbability = 0.005;
}

/**
 * @brief Set environmental parameters for Summer.
 *
 * Sets the season to Summer and adjusts VHD, Myxomatose, and death probabilities
 * based on the number of occurrences in the rabbit population.
 *
 * @param numVHD The number of rabbits affected by VHD.
 * @param numMyxomatose The number of rabbits affected by Myxomatose.
 * @param numRabbit The total number of rabbits in the population.
 */
void Environment::setSummer(int numVHD, int numMyxomatose, int numRabbit)
{
    season = 2;
    MyxomatoseProbability = 0.02 + double(numMyxomatose / numRabbit);
    VHDProbability = 0.001 + double(numVHD / numRabbit);
    deathProbability = 0.01;
}

/**
 * @brief Set environmental parameters for Autumn.
 *
 * Sets the season to Autumn and adjusts VHD, Myxomatose, and death probabilities
 * based on the number of occurrences in the rabbit population.
 *
 * @param numVHD The number of rabbits affected by VHD.
 * @param numMyxomatose The number of rabbits affected by Myxomatose.
 * @param numRabbit The total number of rabbits in the population.
 */
void Environment::setAutumn(int numVHD, int numMyxomatose, int numRabbit)
{
    season = 3;
    MyxomatoseProbability = 0.001 + double(numMyxomatose / numRabbit);
    VHDProbability = 0.001 + double(numVHD / numRabbit);
    deathProbability = 0.02;
}

/**
 * @brief Get the probability of VHD in the environment.
 *
 * Returns the probability of VHD (Viral Hemorrhagic Disease) in the current environment.
 *
 * @return The VHD probability.
 */
double Environment::getVhdProbability() const
{
    return VHDProbability;
}

/**
 * @brief Get the probability of Myxomatose in the environment.
 *
 * Returns the probability of Myxomatose in the current environment.
 *
 * @return The Myxomatose probability.
 */
double Environment::getMyxomatoseProbability() const
{
    return MyxomatoseProbability;
}

/**
 * @brief Get the general death probability in the environment.
 *
 * Returns the general death probability in the current environment.
 *
 * @return The death probability.
 */
double Environment::getDeathProbability() const
{
    return deathProbability;
}

