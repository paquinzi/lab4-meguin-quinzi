/**
 * @file RandomGenerator.cpp
 * @brief Implementation of the RandomGenerator class.
 */

#include "RandomGenerator.h"

/**
 * @brief Seed value for the random number generator.
 */
int RandomGenerator::tk = 0;

/**
 * @brief Constructor for the RandomGenerator class.
 *
 * Initializes the random number generator with a unique seed value.
 */
RandomGenerator::RandomGenerator() {
    generator.seed(tk++);
}

/**
 * @brief Generate a random integer within the specified range.
 *
 * @param min The minimum value of the range (inclusive).
 * @param max The maximum value of the range (inclusive).
 * @return A randomly generated integer within the specified range.
 */
int RandomGenerator::genrand_int(int min, int max) {
    std::uniform_int_distribution<int> distribution(min, max);
    return distribution(generator);
}

/**
 * @brief Generate a random real number within the specified range.
 *
 * @param min The minimum value of the range.
 * @param max The maximum value of the range.
 * @return A randomly generated real number within the specified range.
 */
double RandomGenerator::genrand_real(double min, double max) {
    std::uniform_real_distribution<double> distribution(min, max);
    return distribution(generator);
}

/**
 * @brief Generate a random number from an exponential distribution.
 *
 * @param inMean The mean of the exponential distribution.
 * @return A randomly generated number from the exponential distribution.
 */
double RandomGenerator::negExp(double inMean) {
    return -inMean * log(1 - genrand_real(0.0, 1.0));
}

/**
 * @brief Generate a random number from a normal distribution.
 *
 * @param mean The mean of the normal distribution.
 * @param stdDev The standard deviation of the normal distribution.
 * @return A randomly generated number from the normal distribution.
 */
double RandomGenerator::normal(double mean, double stdDev) {
    std::normal_distribution<double> distribution(mean, stdDev);
    return distribution(generator);
}

/**
 * @brief Get the underlying random number generator.
 *
 * @return The std::mt19937 random number generator.
 */
std::mt19937 RandomGenerator::getGenerator() {
    return generator;
}
