/**
 * @file Simulation.cpp
 * @brief Implementation of the Simulation class methods.
 *
 * The Simulation class models the population dynamics of rabbits over time.
 * It simulates the effects of aging, diseases, predation, and environmental factors.
 */

#include "Simulation.h"

using namespace std;

/**
 * @brief Constructor for the Simulation class.
 *
 * Initializes a simulation with an initial population of rabbits.
 *
 * @param initialPopulation The initial population of rabbits.
 * @param random A random number generator for the simulation.
 */
Simulation::Simulation(int initialPopulation, RandomGenerator& random) :  rng(random), year(0),
numRabbit(initialPopulation), numDeath(0), numBirth(0), numMyxomatose(0), numVHD(0), numPregnant(0),
numAdult(initialPopulation), deathAge{0}, nblitter{0}{
    numFemale = 0;
    numFertile = 0;
    for(int i = 0; i < initialPopulation; i++)
    {
        auto newRabbit = new Rabbit(rng, 12);
        if (newRabbit->isFemale()) {
            numFemale++;
            numFertile++;
        }
        rabbits.emplace_back(newRabbit);
    }
}

/**
 * @brief Print statistics for the simulation at a given month.
 *
 * Outputs various statistics including births, deaths, and rabbit demographics
 * for a specific month in the simulation.
 *
 * @param month The month for which statistics are to be printed.
 *              (0 for initial population, 1 for January, 2 for February, ..., 12 for December)
 */
void Simulation::printStatistics(int month) const
{
    // Check if it's the initial population
    if (month == 0)
        cout << "Initial population" << endl << endl;
    else
        // Output the month and year
        cout << Month [month -1] << ", " << 2023 + year << endl << endl;

    // Output various statistics
    cout << "numBirth: " << numBirth << endl;
    cout << "numDeath: " << numDeath << endl;
    cout << "numPregnant: " << numPregnant << endl;
    cout << "numRabbit: " << numRabbit << endl;
    cout << "numAdult: " << numAdult << endl;
    cout << "numFemale: " << numFemale << endl;
    cout << "numFertile: " << numFertile << endl;
    cout << "numMyxomatose: " << numMyxomatose << endl;
    cout << "numVHD: " << numVHD << endl << endl;
}

/**
 * @brief Simulate the population dynamics for a specified number of years.
 *
 * Simulates the population dynamics for the specified number of years.
 * It uses the `simulateAYear` function for each year and increments the
 * simulation year accordingly.
 *
 * @param numYears The number of years to simulate.
 */
void Simulation::simulateNYears(int numYears)
{
    // Create a new Environment for the simulation
    auto* environment = new Environment();

    // Print statistics for the initial state
    //printStatistics(0);

    // Loop through the specified number of years
    for(int i = 0; i < numYears; i++)
    {
        // Simulate a year and update the environment
        simulateAYear(*environment);

        // Increment the simulation year
        year++;
    }

    // Clean up dynamically allocated memory for the Environment
    cout << endl;
    delete environment;
}

/**
 * @brief Simulate the population dynamics for one year.
 *
 * Simulates the population dynamics for each month of the year using the
 * provided environment. It updates the season-specific parameters and
 * prints statistics for each simulated month.
 *
 * @param environment The environment for the simulation.
 */
void Simulation::simulateAYear(Environment& environment)
{
    // Loop through each month of the year
    for(int month = 1; month <= 12; month++) {
        // Adjust season-specific parameters in the environment
        if (month == 1 && environment.getSeason() != 0)
            environment.setWinter(numVHD, numMyxomatose, numRabbit);
        else if (month == 4 && environment.getSeason() != 1)
            environment.setSpring(numVHD, numMyxomatose, numRabbit);
        else if (month == 7 && environment.getSeason() != 2)
            environment.setSummer(numVHD, numMyxomatose, numRabbit);
        else if (month == 10 && environment.getSeason() != 3)
            environment.setAutumn(numVHD, numMyxomatose, numRabbit);

        // Simulate the population dynamics for the month
        simulateAMonth(environment);

        // Print statistics for the simulated month
        printStatistics(month);

        //cout << numRabbit << endl;
        //cout << float(numDeath) / float(numRabbit) << endl;
        //cout << numBirth << endl;
        //cout << numMyxomatose << endl;
        //cout << numVHD << endl;
    }
}

/**
 * @brief Simulate the population dynamics for one month.
 *
 * Simulates the population dynamics for the specified month using the provided environment.
 * It updates the age of each rabbit, handles disease-related deaths, predator attacks,
 * and checks for overpopulation. It also handles births, pregnancies, and updates statistics
 * accordingly.
 *
 * @param environment The environment for the simulation.
 */
void Simulation::simulateAMonth(Environment& environment) {
    // Initialize counters for deaths, births, adults, and fertile rabbits
    numDeath = 0,
    numBirth = 0,
    numAdult = 0,
    numFertile = 0;

    // Iterate through each rabbit in the population
    for (auto it = rabbits.begin(); it != rabbits.end();) {
        Rabbit* rabbit = *it;

        // Age the rabbit
        rabbit->aging();

        // Handle Myxomatose and VHD and update counters
        numMyxomatose += rabbit->handleMyxomatose(environment);
        numVHD += rabbit->handleVHD(environment);

        // Calculate population density
        double populationDensity = static_cast<double>(numRabbit) / 10000000;

        // Check for mortality due to disease, predators, or overpopulation
        if (rabbit->handleMortality(environment) || rabbit->handlePredators()
                                                 || rabbit->handleOverpopulation(populationDensity)) {
            /*
            if (rabbit->isFemale()){
                cout << "nb " << rabbit->getNbLitters() << endl;
                cout << "age " << rabbit->getAge() << endl;
                nblitter[int(rabbit->getNbLitters()/rabbit->getAge())]++;
            }*/
            updateStatistics(rabbit, 'D');
            it = rabbits.erase(it);
            delete rabbit;
        } else {
            // If the rabbit survives, update statistics and handle births, pregnancies
            updateStatistics(rabbit, 'H');

            numBirth += rabbit->handleBirth();

            if(rabbit->handlePregnancy(environment)) {
                rabbit->setPregnant(true);
                numPregnant++;
            }
            ++it;
        }
    }
    // Iterate through the number of births and add new rabbits to the population
    for (int i = 0; i < numBirth; i++) {
        auto newRabbit = new Rabbit(rng);
        updateStatistics(newRabbit, 'B');
        rabbits.emplace_back(newRabbit);
    }
}

/**
 * @brief Update simulation statistics based on rabbit events.
 *
 * Updates various statistics in the simulation based on events related to a specific rabbit.
 * The events include death ('D'), birth ('B'), and other default cases, such as aging and
 * changes in the rabbit's life stage.
 *
 * @param rabbit The rabbit for which statistics are updated.
 * @param option The option indicating the type of event ('D' for death, 'B' for birth, default for other events).
 */
void Simulation::updateStatistics(Rabbit* rabbit, char option) {
    switch (option) {
        case 'D':
            // Update death statistics
            deathAge[rabbit->getAge()]++;
            numDeath++;

            // Decrement counts based on the rabbit's characteristics
            numRabbit--;
            if(rabbit -> isAdult()) numAdult--;
            if(rabbit->isFemale()) numFemale--;
            if(rabbit->isFertile()) numFertile--;
            if (rabbit->isPregnant()) numPregnant--;

            // Decrement disease-specific counts
            if(rabbit->getStatus() == Status::MyxAndVHD || rabbit->getStatus() == Status::Myxomatose)
                numMyxomatose--;
            if(rabbit->getStatus() == Status::MyxAndVHD || rabbit->getStatus() == Status::VHD)
                numVHD--;
            break;

        case 'B':
            // Increment counts for birth event
            numRabbit++;
            if(rabbit->isFemale())
                numFemale++;
            break;
        default:
            // Default case (aging and other life stage changes)
            if(rabbit->isAdult()) numAdult++;
            if(rabbit->isFertile()) numFertile++;
            if(rabbit->isPregnant()) numPregnant--;
    }
}

/**
 * @brief Print the distribution of rabbit ages at the time of death.
 *
 * Outputs the distribution of rabbit ages at the time of death. Each element
 * in the array corresponds to the count of rabbits that died at a specific age.
 */
void Simulation::printDeathAge() const {
    for (int i : deathAge)
        cout << i << endl;
}

/**
 * @brief Print the distribution of litter sizes.
 *
 * Outputs the distribution of litter sizes. Each element in the array corresponds
 * to the count of litters that had a specific size.
 */
void Simulation::printNbLitter() const {
    for (int i: nblitter)
        cout << i << endl;
}
