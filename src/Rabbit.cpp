/**
 * @file Rabbit.cpp
 * @brief Implementation of the Rabbit class methods.
 *
 * The Rabbit class models individual rabbits in the simulation. It includes methods
 * for handling aging, diseases (Myxomatose and VHD), mortality, birth, pregnancy,
 * and interactions with predators and overpopulation. This file contains the
 * implementation of the Rabbit class methods, including the constructor, getters,
 * setters, and methods for handling various aspects of a rabbit's life.
 */

#include "Rabbit.h"
#include "RandomGenerator.h"
using namespace std;

/**
 * @brief Constructor for the Rabbit class.
 *
 * Initializes a rabbit with default values for age, pregnancy status, status,
 * and random gender, ability, and adult age.
 *
 * @param random A random number generator for the simulation.
 */
Rabbit::Rabbit(RandomGenerator &random) : age(0), pregnant(false), nbLitters(0), status(Status::Healthy), rng(random)
{
    gender = rng.genrand_int(0, 1);
    ability =  int(rng.normal(14, 6));
    adultAge = (gender == 1) ? 4 :int(rng.normal(6.5,1.5));
}

/**
 * @brief Constructor for the Rabbit class with a specified age.
 *
 * Initializes a rabbit with a specified age and default values for pregnancy status,
 * status, and random gender and adult age.
 *
 * @param random A random number generator for the simulation.
 * @param age The age of the rabbit.
 */
Rabbit::Rabbit(RandomGenerator& random, int age): age(age), pregnant(false), status(Status::Healthy), rng(random)
{
    gender = rng .genrand_int(0, 1);
    ability =  int(rng.normal(14, 6));
    //adultAge = int(rng.normal(6.5,1.5));
    adultAge = (gender == 1) ? 4 :int(rng.normal(6.5,1.5));
}

/**
 * @brief Get the adult age of the rabbit.
 *
 * Returns the age at which the rabbit is considered an adult.
 *
 * @return The adult age.
 */
int Rabbit::getAdultAge() const
{
    return adultAge;
}

/**
 * @brief Get the current age of the rabbit.
 *
 * Returns the current age of the rabbit.
 *
 * @return The current age.
 */
int Rabbit::getAge() const
{
    return age;
}

/**
 * @brief Check if the rabbit is female.
 *
 * Returns true if the rabbit is female, false otherwise.
 *
 * @return true if the rabbit is female, false otherwise.
 */
bool Rabbit::isFemale() const
{
    return gender == 1;
}

/**
 * @brief Check if the rabbit is an adult.
 *
 * Returns true if the rabbit is an adult, false otherwise.
 *
 * @return true if the rabbit is an adult, false otherwise.
 */
bool Rabbit::isAdult() const
{
    return age >= adultAge;
}

/**
 * @brief Check if the rabbit is fertile.
 *
 * Returns true if the rabbit is fertile (adult and age less than 48), false otherwise.
 *
 * @return true if the rabbit is fertile, false otherwise.
 */
bool Rabbit::isFertile() const
{
    return isAdult() && age < 48;
}

/**
 * @brief Age the rabbit by one month.
 *
 * Increments the age of the rabbit by one month.
 */
void Rabbit::aging()
{
    age++;
}

/**
 * @brief Check if the rabbit is pregnant.
 *
 * Returns true if the rabbit is pregnant, false otherwise.
 *
 * @return true if the rabbit is pregnant, false otherwise.
 */
bool Rabbit::isPregnant() const
{
    return pregnant;
}

/**
 * @brief Set the pregnancy status of the rabbit.
 *
 * Sets the pregnancy status of the rabbit to the specified value.
 *
 * @param isPregnant The pregnancy status to set.
 */
void Rabbit::setPregnant(bool isPregnant)
{
    pregnant = isPregnant;
}

/**
 * @brief Get the disease status of the rabbit.
 *
 * Returns the current disease status of the rabbit.
 *
 * @return The disease status of the rabbit.
 */
Status Rabbit::getStatus() const
{
    return status;
}

/**
 * @brief Handle Myxomatose for the rabbit.
 *
 * Checks if the rabbit is affected by Myxomatose based on the environmental probability.
 * Updates the rabbit's status accordingly.
 *
 * @param environment The environment for the simulation.
 * @return 1 if the rabbit is affected by Myxomatose, 0 otherwise.
 */
int Rabbit::handleMyxomatose(Environment environment)
{
    // Check if the rabbit will get Myxomatose
    if(rng.genrand_real(0,1) < environment.getMyxomatoseProbability()
       && status != Status::Myxomatose && status != Status::MyxAndVHD)
    {
        if (status == Status::VHD)
            status = Status::MyxAndVHD;
        else
            status = Status::Myxomatose;

        // Return 1 to indicate that the rabbit is affected by Myxomatose
        return 1;
    }
    // Return 0 to indicate that the rabbit is not affected by Myxomatose
    return 0;
}

/**
 * @brief Handle VHD for the rabbit.
 *
 * Checks if the rabbit is affected by VHD based on the environmental probability.
 * Updates the rabbit's status accordingly.
 *
 * @param environment The environment for the simulation.
 * @return 1 if the rabbit is affected by VHD, 0 otherwise.
 */
int Rabbit::handleVHD(Environment environment)
{
    // Get the environmental probability of VHD
    double VHDProbability = environment.getVhdProbability();

    // Adjust the probability based on the age of the rabbit
    if(age < 2)  VHDProbability = 0.0002;
    if (age - adultAge < 6 && age > adultAge) VHDProbability += 0.01;

    // Check if the rabbit will get VHD
    if(rng.genrand_real(0,1) < VHDProbability
                && status != Status::VHD && status != Status::MyxAndVHD)
    {
        if (status == Status::Myxomatose)
            status = Status::MyxAndVHD;
        else
            status = Status::VHD;

        // Return 1 to indicate that the rabbit is affected by VHD
        return 1;
    }
    // Return 0 to indicate that the rabbit is not affected by VHD
    return 0;
}

/**
 * @brief Handle general mortality for the rabbit.
 *
 * Checks if the rabbit dies based on the environmental death probability and other factors
 * such as disease status, age, and fertility. Updates the rabbit's status accordingly.
 *
 * @param environment The environment for the simulation.
 * @return true if the rabbit dies, false otherwise.
 */
bool Rabbit::handleMortality(Environment environment)
{
    // Initialize the death probability with the environmental death probability
    double deathProbability = environment.getDeathProbability();

    // Adjust the death probability based on disease status
    if (status == Status::Myxomatose || status == Status::MyxAndVHD) deathProbability += 0.4;
    if (status == Status::VHD || status == Status::MyxAndVHD) deathProbability += 0.4;

    // Adjust the death probability for infant mortality (age < 6)
    if(age < 6)
        deathProbability *= 2;

    // Adjust the death probability for older rabbits (age > 120)
    if(age > 120)
        deathProbability += int(age/12) * 0.1;

    // Return True if the rabbit dies, false otherwise
    return rng.genrand_real(0,1) < deathProbability;
}

/**
 * @brief Handle birth for the rabbit.
 *
 * Checks if the rabbit gives birth based on its pregnancy status. If pregnant,
 * it returns the number of offspring.
 *
 * @return The number of offspring if the rabbit gives birth, 0 otherwise.
 */
int Rabbit::handleBirth()
{
    if (isPregnant())
    {
        setPregnant(false);
        nbLitters++;
        return int(rng.normal(3, 1.2));
    }
    return 0;
}

/**
 * @brief Display information about the rabbit.
 *
 * Outputs information about the rabbit's adult age, gender, adult status,
 * fertile status, and current age.
 */
void Rabbit::display()
{
    std::cout <<  "adultAge " << getAdultAge() << std::endl;
    std::cout << "Female " << isFemale() << std::endl;
    std::cout << "Adult " <<isAdult() << std::endl;
    std::cout << "Fertile " << isFertile() << std::endl;
    std::cout << "Age " << getAge() << std::endl;
}

/**
 * @brief Handle pregnancy for the rabbit.
 *
 * Checks if the rabbit becomes pregnant based on its fertility, gender, and environmental season.
 * Adjusts the pregnancy probability accordingly.
 *
 * @param environment The environment for the simulation.
 * @return true if the rabbit becomes pregnant, false otherwise.
 */
bool Rabbit::handlePregnancy(Environment environment)
{
    double pregnancyProbability = 0;
    if(isFertile() && isFemale())
    {
        if (environment.getSeason() == 1)
            pregnancyProbability = 0.6;
        else
            pregnancyProbability = 0.4;
    }
    return rng.genrand_real(0,1) < pregnancyProbability;
}

/**
 * @brief Handle predator attacks for the rabbit.
 *
 * Checks if the rabbit is attacked by predators based on a random probability.
 * Adjusts the death probability based on the rabbit's ability.
 *
 * @return true if the rabbit is attacked by predators, false otherwise.
 */
bool Rabbit::handlePredators()
{
    if(rng.genrand_real(0,1) < 0.1)
    {
        double deathByPred = (20 - ability) * 0.1;

        if(rng.genrand_real(0,1) < deathByPred)
            return true;
    }
    return false;
}

/**
 * @brief Handle death due to overpopulation for the rabbit.
 *
 * Checks if the rabbit dies due to overpopulation based on the given population density.
 * Adjusts the death probability accordingly.
 *
 * @param populationDensity The current population density.
 * @return true if the rabbit dies due to overpopulation, false otherwise.
 */
bool Rabbit::handleOverpopulation(double populationDensity)
{
    // Calculate the overpopulation death probability
    double overpopulationDeathProbability = std::max(0.0, populationDensity - 1.0);

    if (rng.genrand_real(0, 1) < overpopulationDeathProbability)
        return true;
    return false;
}

/**
 * @brief Get the number of litters for the rabbit.
 *
 * Returns the number of litters for the rabbit.
 *
 * @return The number of litters.
 */
int Rabbit::getNbLitters() const{
    return nbLitters;
}

