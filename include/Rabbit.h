#ifndef RABBIT_H
#define RABBIT_H

#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>

#include "Environment.h"
#include "RandomGenerator.h"

enum class Status{
    Healthy,
    Myxomatose,
    VHD,
    MyxAndVHD,
};

class Rabbit {
public:
    /**
     * @brief Constructor for the Rabbit class.
     *
     * Initializes a rabbit with default values for age, pregnancy status, status,
     * and random gender, ability, and adult age.
     *
     * @param random A random number generator for the simulation.
     */
    explicit Rabbit(RandomGenerator &random);

    /**
     * @brief Constructor for the Rabbit class with a specified age.
     *
     * Initializes a rabbit with a specified age and default values for pregnancy status,
     * status, and random gender and adult age.
     *
     * @param random A random number generator for the simulation.
     * @param age The age of the rabbit.
     */
    Rabbit(RandomGenerator &random, int age);

    /**
     * @brief Check if the rabbit is an adult.
     *
     * Returns true if the rabbit is an adult, false otherwise.
     *
     * @return true if the rabbit is an adult, false otherwise.
     */
    bool isAdult() const;

    /**
     * @brief Check if the rabbit is female.
     *
     * Returns true if the rabbit is female, false otherwise.
     *
     * @return true if the rabbit is female, false otherwise.
     */
    bool isFemale() const;

    /**
     * @brief Check if the rabbit is fertile.
     *
     * Returns true if the rabbit is fertile (adult and age less than 48), false otherwise.
     *
     * @return true if the rabbit is fertile, false otherwise.
     */
    bool isFertile() const;

    /**
     * @brief Check if the rabbit is pregnant.
     *
     * Returns true if the rabbit is pregnant, false otherwise.
     *
     * @return true if the rabbit is pregnant, false otherwise.
     */
    bool isPregnant() const;

    /**
     * @brief Get the adult age of the rabbit.
     *
     * Returns the age at which the rabbit is considered an adult.
     *
     * @return The adult age.
     */
    int getAdultAge() const;

    /**
     * @brief Get the current age of the rabbit.
     *
     * Returns the current age of the rabbit.
     *
     * @return The current age.
     */
    int getAge() const;

    /**
     * @brief Get the disease status of the rabbit.
     *
     * Returns the current disease status of the rabbit.
     *
     * @return The disease status of the rabbit.
     */
    Status getStatus() const;

    /**
     * @brief Set the pregnancy status of the rabbit.
     *
     * Sets the pregnancy status of the rabbit to the specified value.
     *
     * @param isPregnant The pregnancy status to set.
     */
    void setPregnant(bool pregnant);

    /**
     * @brief Handle birth for the rabbit.
     *
     * Checks if the rabbit gives birth based on its pregnancy status. If pregnant,
     * it returns the number of offspring.
     *
     * @return The number of offspring if the rabbit gives birth, 0 otherwise.
     */
    int handleBirth();

    /**
     * @brief Handle Myxomatose for the rabbit.
     *
     * Checks if the rabbit is affected by Myxomatose based on the environmental probability.
     * Updates the rabbit's status accordingly.
     *
     * @param environment The environment for the simulation.
     * @return 1 if the rabbit is affected by Myxomatose, 0 otherwise.
     */
    int handleMyxomatose(Environment environment);

    /**
     * @brief Handle VHD for the rabbit.
     *
     * Checks if the rabbit is affected by VHD based on the environmental probability.
     * Updates the rabbit's status accordingly.
     *
     * @param environment The environment for the simulation.
     * @return 1 if the rabbit is affected by VHD, 0 otherwise.
     */
    int handleVHD(Environment environment);

    /**
     * @brief Handle general mortality for the rabbit.
     *
     * Checks if the rabbit dies based on the environmental death probability and other factors
     * such as disease status, age, and fertility. Updates the rabbit's status accordingly.
     *
     * @param environment The environment for the simulation.
     * @return true if the rabbit dies, false otherwise.
     */
    bool handleMortality(Environment environment);

    /**
     * @brief Handle pregnancy for the rabbit.
     *
     * Checks if the rabbit becomes pregnant based on its fertility, gender, and environmental season.
     * Adjusts the pregnancy probability accordingly.
     *
     * @param environment The environment for the simulation.
     * @return true if the rabbit becomes pregnant, false otherwise.
     */
    bool handlePregnancy(Environment environment);

    /**
     * @brief Handle predator attacks for the rabbit.
     *
     * Checks if the rabbit is attacked by predators based on a random probability.
     * Adjusts the death probability based on the rabbit's ability.
     *
     * @return true if the rabbit is attacked by predators, false otherwise.
     */
    bool handlePredators();

    /**
     * @brief Handle death due to overpopulation for the rabbit.
     *
     * Checks if the rabbit dies due to overpopulation based on the given population density.
     * Adjusts the death probability accordingly.
     *
     * @param populationDensity The current population density.
     * @return true if the rabbit dies due to overpopulation, false otherwise.
     */
    bool handleOverpopulation(double populationDensity);

    /**
     * @brief Age the rabbit by one month.
     *
     * Increments the age of the rabbit by one month.
     */
    void aging();

    /**
     * @brief Display information about the rabbit.
     *
     * Outputs information about the rabbit's adult age, gender, adult status,
     * fertile status, and current age.
     */
    void display();

    int getNbLitters() const;


private:
    int age; // in months
    int adultAge;
    int gender;
    bool pregnant;
    int ability;
    int nbLitters;
    Status status;
    RandomGenerator& rng;
};

#endif
