#ifndef RANDOM_GENERATOR_H
#define RANDOM_GENERATOR_H

#include <random>

class RandomGenerator {
public:
    /**
     * @brief Constructor for the RandomGenerator class.
     *
     * Initializes the random number generator with a unique seed value.
     */
    RandomGenerator();

    /**
     * @brief Generate a random integer within the specified range.
     *
     * @param min The minimum value of the range (inclusive).
     * @param max The maximum value of the range (inclusive).
     * @return A randomly generated integer within the specified range.
     */
    int genrand_int(int min, int max);

    /**
     * @brief Generate a random real number within the specified range.
     *
     * @param min The minimum value of the range.
     * @param max The maximum value of the range.
     * @return A randomly generated real number within the specified range.
     */
    double genrand_real(double min, double max);

    /**
     * @brief Generate a random number from an exponential distribution.
     *
     * @param inMean The mean of the exponential distribution.
     * @return A randomly generated number from the exponential distribution.
     */
    double negExp(double mean);

    /**
     * @brief Generate a random number from a normal distribution.
     *
     * @param mean The mean of the normal distribution.
     * @param stdDev The standard deviation of the normal distribution.
     * @return A randomly generated number from the normal distribution.
     */
    double normal(double mean, double stdDev);

    /**
     * @brief Get the underlying random number generator.
     *
     * @return The std::mt19937 random number generator.
     */
    std::mt19937 getGenerator();

    int static tk;


private:
    std::mt19937 generator;
};



#endif
