#ifndef SIMULATION_H
#define SIMULATION_H

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
#include <array>
#include <list>

#include "Environment.h"
#include "RandomGenerator.h"
#include "Rabbit.h"

class Simulation {
public:
    /**
     * @brief Array containing the names of months.
     */
    static constexpr std::array<const char*, 12> Month = {
            "January", "February", "March", "April",
            "May", "June", "July", "August",
            "September", "October", "November", "December"
    };

    /**
     * @brief Constructor for the Simulation class.
     *
     * Initializes a simulation with an initial population of rabbits.
     *
     * @param initialPopulation The initial population of rabbits.
     * @param random A random number generator for the simulation.
     */
    Simulation(int initialPopulation, RandomGenerator& random);

    /**
     * @brief Simulate the population dynamics for a specified number of years.
     *
     * Simulates the population dynamics for the specified number of years.
     * It uses the `simulateAYear` function for each year and increments the
     * simulation year accordingly.
     *
     * @param numYears The number of years to simulate.
     */
    void simulateNYears(int numYears);

    /**
     * @brief Print statistics for the simulation at a given month.
     *
     * Outputs various statistics including births, deaths, and rabbit demographics
     * for a specific month in the simulation.
     *
     * @param month The month for which statistics are to be printed.
     *              (0 for initial population, 1 for January, 2 for February, ..., 12 for December)
     */
    void printStatistics(int month) const;

    /**
     * @brief Print the distribution of rabbit ages at the time of death.
     *
     * Outputs the distribution of rabbit ages at the time of death. Each element
     * in the array corresponds to the count of rabbits that died at a specific age.
     */
    void printDeathAge() const;

    /**
     * @brief Print the distribution of litter sizes.
     *
     * Outputs the distribution of litter sizes. Each element in the array corresponds
     * to the count of litters that had a specific size.
     */
    void printNbLitter() const;

private:
        std::list<Rabbit*>  rabbits;
        RandomGenerator     rng;
        int                 year,
                            numRabbit,
                            numDeath,
                            numBirth,
                            numMyxomatose,
                            numVHD,
                            numFertile,
                            numFemale,
                            numPregnant,
                            numAdult,
                            deathAge[120] = {0},
                            nblitter[120] = {0};

    /**
    * @brief Simulate the population dynamics for one year.
    *
    * Simulates the population dynamics for each month of the year using the
    * provided environment. It updates the season-specific parameters and
    * prints statistics for each simulated month.
    *
    * @param environment The environment for the simulation.
    */
    void simulateAYear(Environment& environment);

    /**
     * @brief Simulate the population dynamics for one month.
     *
     * Simulates the population dynamics for the specified month using the provided environment.
     * It updates the age of each rabbit, handles disease-related deaths, predator attacks,
     * and checks for overpopulation. It also handles births, pregnancies, and updates statistics
     * accordingly.
     *
     * @param environment The environment for the simulation.
     */
    void simulateAMonth(Environment& environment);

    /**
     * @brief Update simulation statistics based on rabbit events.
     *
     * Updates various statistics in the simulation based on events related to a specific rabbit.
     * The events include death ('D'), birth ('B'), and other default cases, such as aging and
     * changes in the rabbit's life stage.
     *
     * @param rabbit The rabbit for which statistics are updated.
     * @param option The option indicating the type of event ('D' for death, 'B' for birth, default for other events).
     */
    void updateStatistics(Rabbit* rabbit, char option);
};


#endif
