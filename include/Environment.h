#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

class Environment {
public :
        Environment();
    /**
     * @brief Set environmental parameters for Winter.
     *
     * Sets the season to Winter and adjusts VHD, Myxomatose, and death probabilities
     * based on the number of occurrences in the rabbit population.
     *
     * @param numVHD The number of rabbits affected by VHD.
     * @param numMyxomatose The number of rabbits affected by Myxomatose.
     * @param numRabbit The total number of rabbits in the population.
     */
    void setWinter(int numVHD, int numMyxomatose, int numRabbit);

    /**
     * @brief Set environmental parameters for Spring.
     *
     * Sets the season to Spring and adjusts VHD, Myxomatose, and death probabilities
     * based on the number of occurrences in the rabbit population.
     *
     * @param numVHD The number of rabbits affected by VHD.
     * @param numMyxomatose The number of rabbits affected by Myxomatose.
     * @param numRabbit The total number of rabbits in the population.
     */
    void setSpring(int numVHD, int numMyxomatose, int numRabbit);

    /**
     * @brief Set environmental parameters for Summer.
     *
     * Sets the season to Summer and adjusts VHD, Myxomatose, and death probabilities
     * based on the number of occurrences in the rabbit population.
     *
     * @param numVHD The number of rabbits affected by VHD.
     * @param numMyxomatose The number of rabbits affected by Myxomatose.
     * @param numRabbit The total number of rabbits in the population.
     */
    void setSummer(int numVHD, int numMyxomatose, int numRabbit);

    /**
     * @brief Set environmental parameters for Autumn.
     *
     * Sets the season to Autumn and adjusts VHD, Myxomatose, and death probabilities
     * based on the number of occurrences in the rabbit population.
     *
     * @param numVHD The number of rabbits affected by VHD.
     * @param numMyxomatose The number of rabbits affected by Myxomatose.
     * @param numRabbit The total number of rabbits in the population.
     */
    void setAutumn(int numVHD, int numMyxomatose, int numRabbit);

    /**
    * @brief Get the current season.
    *
    * Returns the current season, where 0 represents Winter, 1 represents Spring,
    * 2 represents Summer, and 3 represents Autumn.
    *
    * @return The current season.
    */
    int getSeason() const;

    /**
     * @brief Get the probability of VHD in the environment.
     *
     * Returns the probability of VHD (Viral Hemorrhagic Disease) in the current environment.
     *
     * @return The VHD probability.
     */
    double getVhdProbability() const;

    /**
     * @brief Get the probability of Myxomatose in the environment.
     *
     * Returns the probability of Myxomatose in the current environment.
     *
     * @return The Myxomatose probability.
     */
    double getMyxomatoseProbability() const;

    /**
     * @brief Get the general death probability in the environment.
     *
     * Returns the general death probability in the current environment.
     *
     * @return The death probability.
     */
    double getDeathProbability() const;

    private:
        int season;
        double VHDProbability;
        double MyxomatoseProbability;
        double deathProbability;
};


#endif
