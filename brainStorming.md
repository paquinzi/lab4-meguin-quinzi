# Données
## Mortalité

- Le taux de mortalité prends en compte les maladies et les prédateurs
- Le taux de mortalité prends en compte l'age -> date de naissance et de mort

- Pour les adultes (lapins à maturité sexuelle), le taux de survie est de 60 %,
- Pour les petits lapins il n'est que de 35 %.
- Lorsqu'un lapin atteint l'âge de 10 ans, son taux de survie est diminué de 10% chaque année,
  pour atteindre 0% à la fin de l'année 15 (décès d'un vieux lapin vénérable).

- On peut simuler ce modèle avec des paramètres d'entrées et des lois de probabilités **à choisir**

## Reproduction / Naissance


- Une femelle peut mettre bas environ tous les mois. Elle fait entre 4 et 8 portées par an.
    Mais ce n'est pas uniforme. + de chance d'avoir 5, 6 ou 7. -> **Gausienne**
- Pour chaque portée il y a une chance égale d'avoir entre 3 et 6 bébés
- L'aléatoire détermine si c'est un mâle ou une femelle (environ 50 / 50)

- La maturité sexuelle est atteinte entre 5 à 8 mois après la naissance des bébés, mais
vous pouvez simplifier et conserver un pas de temps annuel pour la simulation.

- age non fécondité mâle    : 5 à 6 ans
- age non fécondité femelle : 4 ans

## Espace

- Grille ?? static vs dynamique
- déplacement around vs déplacement aléatoire

## Lapins

lien étude : https://www.federaciongalegadecaza.com/biblioteca/coello/REVISTASCAZA_013.pdf
- stats ?? (Vitesse / Vision / ...)
- enum maladies {myxomatose, VHD}
- myxomatose plus probable dans zone de moustiques 
- VHD -> touche très rarement les lapins de moins de 2 mois
      -> touche en générale les lapins approchant de l'age adulte

## Prédateurs

- comportement des prédateurs
- enum predateur = {canidé, félin, rapace, homme}

## Idees bonus

- nourritures
- saison 

